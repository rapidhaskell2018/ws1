# RapidHaskell Workshop 1

This is the output of Workshop 1 with some minor additions.

* Project name is `ws1` not `foo`, and the executable is `ws1-exe`
* Linting and test run are in the CI in `.gitlab-ci.yml`
* Prints a few more things in different colors
* Adds a .ghci file which runs when /app or /src changes when using ghcid

To build

```{.bash}
export RIO_VERBOSE=true
stack build
stack exec -- ws1-exe
```

Slides are [here](http://rapidhaskell2018.gitlab.io/workshops/1.pdf)
