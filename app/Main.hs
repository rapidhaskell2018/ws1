module Main where

import RIO

main :: IO ()
main = runSimpleApp $ do
  logInfo "Hello, World!"
  logDebug "Goodbye"
  logWarn "What the..."
  logError "OHNO!"
